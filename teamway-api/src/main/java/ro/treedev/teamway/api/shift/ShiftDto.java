/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.api.shift;

import java.time.OffsetDateTime;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString(callSuper = false)
@Accessors(chain = true)
public class ShiftDto {

  private String id;

  private String workerId;

  private OffsetDateTime startTime;

  private OffsetDateTime endTime;

}
