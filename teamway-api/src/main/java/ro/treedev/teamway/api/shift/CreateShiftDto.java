/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.api.shift;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.time.OffsetDateTime;

import javax.validation.Constraint;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import ro.treedev.teamway.api.shift.CreateShiftDto.ValidCreateShiftDto;
import ro.treedev.teamway.core.validator.AbstractConstraintValidator;

@Data
@ToString(callSuper = false)
@Accessors(chain = true)
@ValidCreateShiftDto
public class CreateShiftDto {

  @ApiModelProperty(required = true, allowEmptyValue = false, example = "2021-02-05T00:00:00.000000+03:00")
  private OffsetDateTime startTime;

  @ApiModelProperty(required = true, allowEmptyValue = false, example = "2021-02-05T08:00:00.000000+03:00")
  private OffsetDateTime endTime;

  /**
   * Validates an {@link CreateShiftDto}.
   * 
   * @author Arnold Robert Turdean
   */
  @Documented
  @Target({ ElementType.TYPE })
  @Retention(RetentionPolicy.RUNTIME)
  @Constraint(validatedBy = { ValidCreateShiftDto.Validator.class })
  public @interface ValidCreateShiftDto {

    /**
     * Get message.
     * 
     * @return
     */
    String message() default "Invalid CreateShiftDto";

    /**
     * Get groups.
     * 
     * @return
     */
    Class<?>[] groups() default {};

    /**
     * Get payload.
     * 
     * @return
     */
    Class<? extends Payload>[] payload() default {};

    /**
     * Validator.
     * 
     * @author Arnold Robert Turdean
     */
    public static class Validator extends
        AbstractConstraintValidator<ValidCreateShiftDto, CreateShiftDto> {

      /**
       * @see ro.treedev.commons.validator.AbstractConstraintValidator#validate(java.lang.Object,
       *      javax.validation.ConstraintValidatorContext)
       */
      @Override
      public boolean validate(CreateShiftDto entity, ConstraintValidatorContext context) {
        if (entity == null) {
          return this.addValueShouldBeMentionedError(context);
        }

        if (!entity.getStartTime().isBefore(entity.getEndTime())) {
          return this.addError("The start time must be before the end time", context);
        }

        return true;
      }

    }
  }

}
