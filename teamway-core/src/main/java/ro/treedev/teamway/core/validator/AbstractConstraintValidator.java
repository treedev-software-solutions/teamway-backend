/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.core.validator;

import java.lang.annotation.Annotation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.hibernate.validator.internal.engine.constraintvalidation.ConstraintValidatorContextImpl;

/**
 * The {@link AbstractConstraintValidator} gives some common features for the concrete validators in
 * order to make easier the validation. The implementation automatically dissables the default
 * constraint violation.
 * 
 * @author Arnold Robert Turdean
 * @param <A>
 * @param <T>
 */
public abstract class AbstractConstraintValidator<A extends Annotation, T> implements
    ConstraintValidator<A, T> {

  /** constraintAnnotation */
  protected A constraintAnnotation;

  /**
   * @see javax.validation.ConstraintValidator#initialize(java.lang.annotation.Annotation)
   */
  @Override
  public void initialize(A constraintAnnotation) {
    this.constraintAnnotation = constraintAnnotation;
  }

  /**
   * @see javax.validation.ConstraintValidator#isValid(java.lang.Object,
   *      javax.validation.ConstraintValidatorContext)
   */
  @Override
  public final boolean isValid(T value, ConstraintValidatorContext context) {
    context.disableDefaultConstraintViolation();

    return this.validate(value, context);
  }

  /**
   * Validate the given value.
   * 
   * @param value
   * @param context
   * @return
   */
  public abstract boolean validate(T value, ConstraintValidatorContext context);

  /**
   * Get the field's name.
   * 
   * @param context
   * @param defaultValue
   * @return
   */
  protected String getFieldName(ConstraintValidatorContext context, String defaultValue) {
    if (context instanceof ConstraintValidatorContextImpl) {
      return ((ConstraintValidatorContextImpl) context).getConstraintViolationCreationContexts().get(
          0).getPath().asString();
    }

    return defaultValue;
  }

  /**
   * Add a custom constraint violation.
   * 
   * @param message
   * @param context
   * @return always returns <code>false</code>
   */
  protected boolean addError(String message, ConstraintValidatorContext context) {
    context.buildConstraintViolationWithTemplate(message).addConstraintViolation();
    return false;
  }

  /**
   * Add a 'The value should be mentioned' constraint violation.
   * 
   * @param context
   * @return always returns <code>false</code>
   */
  protected boolean addValueShouldBeMentionedError(ConstraintValidatorContext context) {
    context.buildConstraintViolationWithTemplate(
        "The value should be mentioned").addConstraintViolation();
    return false;
  }

  /**
   * Add 'At least one data should be mentioned' constraint violation.
   * 
   * @param context
   * @return
   * @return always returns <code>false</code>
   */
  protected boolean addEmptyCollectionError(ConstraintValidatorContext context) {
    context.buildConstraintViolationWithTemplate(
        "At least one data should be mentioned").addConstraintViolation();
    return false;
  }

}
