/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.core.error;

import java.util.StringJoiner;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * Runtime exception which holds a value.
 * 
 * @author Arnold Robert Turdean
 */
@Data
@ToString(callSuper = true)
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class ValuedRuntimeException extends RuntimeException {

  private static final StringJoiner MESSAGE_VALUE_JOINER = new StringJoiner(" | Value: ");

  /** serialVersionUID */
  private static final long serialVersionUID = 1L;

  /** value */
  private final String value;

  /**
   * Constructs a new instance.
   * 
   * @param message
   * @param value
   * @param t
   */
  public ValuedRuntimeException(String message, String value, Throwable t) {
    super(MESSAGE_VALUE_JOINER.add(message).add(value).toString(), t);
    this.value = value;
  }

  /**
   * Constructs a new instance.
   * 
   * @param message
   * @param value
   */
  public ValuedRuntimeException(String message, String value) {
    super(MESSAGE_VALUE_JOINER.add(message).add(value).toString());
    this.value = value;
  }

}
