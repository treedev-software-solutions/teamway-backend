/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.dal.converter;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Date;

import org.springframework.core.convert.converter.Converter;

/**
 * {@link Date} to {@link OffsetDateTime} converter.
 * 
 * @author Arnold Robert Turdean
 */
public class DateToOffsetDateTimeConverter implements Converter<Date, OffsetDateTime> {

  /** INSTANCE */
  public static final DateToOffsetDateTimeConverter INSTANCE = new DateToOffsetDateTimeConverter();

  /**
   * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
   */
  @Override
  public OffsetDateTime convert(Date source) {
    return source == null ? null : source.toInstant().atOffset(ZoneOffset.UTC);
  }
}
