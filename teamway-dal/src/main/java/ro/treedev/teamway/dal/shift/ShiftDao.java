/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.dal.shift;

import java.time.OffsetDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString(callSuper = false)
@Accessors(chain = true)
@Document(collection = ShiftRepository.REPOSITORY_ID)
public class ShiftDao {

  @Id
  private String id;

  @Indexed
  private String workerId;

  private OffsetDateTime startTime;

  private OffsetDateTime endTime;

}
