/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.dal.shift;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

@CacheConfig(cacheNames = ShiftRepository.REPOSITORY_ID)
public interface ShiftRepository extends MongoRepository<ShiftDao, String>, CustomShiftRepository {

  /** REPOSITORY_ID */
  public static final String REPOSITORY_ID = "shift";

  Stream<ShiftDao> findAllByWorkerId(String workerId);

  @Query("{}")
  Stream<ShiftDao> getAll();

  @CacheEvict(allEntries = true, beforeInvocation = true)
  void deleteAllByWorkerId(String workerId);

  @Override
  @CacheEvict(allEntries = true, beforeInvocation = true)
  <S extends ShiftDao> S save(S entity);

  @Override
  @Cacheable
  Optional<ShiftDao> findById(String id);

  @Override
  @CacheEvict(allEntries = true, beforeInvocation = true)
  void deleteById(String id);

  @Override
  @CacheEvict(allEntries = true, beforeInvocation = true)
  void delete(ShiftDao entity);

  @Override
  @CacheEvict(allEntries = true, beforeInvocation = true)
  void deleteAll(Iterable<? extends ShiftDao> entities);

  @Override
  @CacheEvict(allEntries = true, beforeInvocation = true)
  void deleteAll();

  @Override
  @CacheEvict(allEntries = true, beforeInvocation = true)
  <S extends ShiftDao> List<S> saveAll(Iterable<S> entities);

}
