/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.dal.worker;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString(callSuper = false)
@Accessors(chain = true)
@Document(collection = WorkerRepository.REPOSITORY_ID)
public class WorkerDao {

  @Id
  private String id;

  private String name;

}
