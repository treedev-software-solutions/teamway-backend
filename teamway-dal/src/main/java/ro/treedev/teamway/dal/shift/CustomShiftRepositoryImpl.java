/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.dal.shift;

import java.time.OffsetDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.google.common.base.Preconditions;

public class CustomShiftRepositoryImpl implements CustomShiftRepository {

  private static final String WORKER_ID = "workerId";

  private static final String END_TIME = "endTime";

  private static final String START_TIME = "startTime";

  @Autowired
  private MongoOperations mongoOperation;

  /**
   * @see ro.treedev.teamway.dal.shift.CustomShiftRepository#getShiftsIntersectingWith(java.time.OffsetDateTime,
   *      java.time.OffsetDateTime)
   */
  @Override
  public List<ShiftDao> getShiftsIntersectingWith(String workerId, OffsetDateTime from,
      OffsetDateTime to) {
    Preconditions.checkNotNull(workerId);
    Preconditions.checkNotNull(from);
    Preconditions.checkNotNull(to);

    final Criteria schedulesAfter = new Criteria().orOperator( //
        Criteria.where(START_TIME).gte(from), //
        Criteria.where(END_TIME).gt(from));

    final Criteria scheduledBefore = new Criteria().orOperator( //
        Criteria.where(START_TIME).lte(to), //
        Criteria.where(END_TIME).lt(to));

    final Criteria workerCriteria = Criteria.where(WORKER_ID).is(workerId);

    final Criteria criteria = new Criteria(). //
        andOperator(workerCriteria, schedulesAfter, scheduledBefore);

    final Query query = new Query().addCriteria(criteria);
    return this.mongoOperation.find(query, ShiftDao.class);
  }

}
