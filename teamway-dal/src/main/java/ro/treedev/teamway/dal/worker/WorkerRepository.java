/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.dal.worker;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

@CacheConfig(cacheNames = WorkerRepository.REPOSITORY_ID)
public interface WorkerRepository extends MongoRepository<WorkerDao, String> {

  /** REPOSITORY_ID */
  public static final String REPOSITORY_ID = "worker";

  @Override
  @CacheEvict(allEntries = true, beforeInvocation = true)
  <S extends WorkerDao> S save(S entity);

  @Query("{}")
  Stream<WorkerDao> getAll();

  @Override
  @Cacheable
  Optional<WorkerDao> findById(String id);

  @Override
  @CacheEvict(allEntries = true, beforeInvocation = true)
  void deleteById(String id);

  @Override
  @CacheEvict(allEntries = true, beforeInvocation = true)
  void delete(WorkerDao entity);

  @Override
  @CacheEvict(allEntries = true, beforeInvocation = true)
  void deleteAll(Iterable<? extends WorkerDao> entities);

  @Override
  @CacheEvict(allEntries = true, beforeInvocation = true)
  void deleteAll();

  @Override
  @CacheEvict(allEntries = true, beforeInvocation = true)
  <S extends WorkerDao> List<S> saveAll(Iterable<S> entities);

}
