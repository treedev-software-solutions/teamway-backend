/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.dal.shift;

import java.time.OffsetDateTime;
import java.util.List;

public interface CustomShiftRepository {

  /**
   * Returns all the shifts which intersects with the given time period.
   * 
   * @param workerId 
   * @param from
   * @param to
   * @return
   */
  List<ShiftDao> getShiftsIntersectingWith(String workerId, OffsetDateTime from, OffsetDateTime to);

}
