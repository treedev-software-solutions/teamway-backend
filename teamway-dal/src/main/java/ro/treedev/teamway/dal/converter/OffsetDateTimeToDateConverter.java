/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.dal.converter;

import java.time.OffsetDateTime;
import java.util.Date;

import org.springframework.core.convert.converter.Converter;

/**
 * {@link OffsetDateTime} to {@link Date} converter.
 * 
 * @author Arnold Robert Turdean
 */
public class OffsetDateTimeToDateConverter implements Converter<OffsetDateTime, Date> {

  /** INSTANCE */
  public static final OffsetDateTimeToDateConverter INSTANCE = new OffsetDateTimeToDateConverter();

  /**
   * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
   */
  @Override
  public Date convert(OffsetDateTime source) {
    return source == null ? null : Date.from(source.toInstant());
  }

}
