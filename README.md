# TeamWay Backend Test Project

The project contains a Spring Boot application based on the following requirements:

	Build a REST application from scratch that could serve as a work planning service.
	
	- A worker has shifts
	- A shift is 8 hours long
	- A worker never has two shifts on the same day
	- It is a 24 hour timetable 0-8, 8-16, 16-24
	- Preferably write a couple of units tests.
	
The implemented application uses an embedded **MongoDB**. This means that even if the data is stored into a MongoDB database the "database will be 100% empty" after each run.

## Requirement 

* JDK 11

## How to run the application

Unzip the related zip file (mentioned in the email) and run the application by the following command:

	java -jar teamway-service-1.0.0-SNAPSHOT.jar

The web application starts and it should accept the requests on the **8091** port.

## Application API

You can check the application's APIs by running the application locally and simply pasting the following link into your browser (Swagger is integrated into the application) :

	http://localhost:8091/swagger-ui.html

## Testing the application

The application contains some **unit tests** as well, but the main focus was put on **Behavior Driven Testing** with Cucumber. You can find the test cases in the "*/src/test/resources/bdt/features*" folder. By running the *CucumberIntegrationTest* class the system will start and all the feature tests in the mentioned folder will be executed one by one. The application will be started once and the database will be cleaned before each scenario.