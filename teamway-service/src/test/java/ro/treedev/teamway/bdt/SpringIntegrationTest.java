/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.bdt;

import java.security.InvalidParameterException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.RestTemplate;

import io.cucumber.spring.CucumberContextConfiguration;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@CucumberContextConfiguration
public abstract class SpringIntegrationTest {

  private final String SERVER_URL = "http://localhost";

  @LocalServerPort
  int port;

  @Autowired
  protected RestTemplate restTemplate;

  @Autowired
  protected CucumberDataCache dataCache;

  protected String computeUrl(String uri) {
    return this.SERVER_URL + ":" + this.port + uri;
  }
  
  protected String getWorkerId(String workerName) {
    return this.dataCache.getWorkerId(workerName).orElseThrow(() -> new InvalidParameterException(
        "The requested workerId was not found!"));
  }

  protected String getShiftId(String shiftName) {
    return this.dataCache.getShiftId(shiftName).orElseThrow(() -> new InvalidParameterException(
        "The requested shiftId was not found!"));
  }

}
