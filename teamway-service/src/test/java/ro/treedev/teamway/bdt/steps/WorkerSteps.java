/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.bdt.steps;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import ro.treedev.teamway.api.controller.WorkerController;
import ro.treedev.teamway.api.worker.CreateWorkerDto;
import ro.treedev.teamway.api.worker.WorkerDto;
import ro.treedev.teamway.bdt.SpringIntegrationTest;

public class WorkerSteps extends SpringIntegrationTest {

  @Given("a worker with name: {word}")
  public void givenWorker(String name) {
    final CreateWorkerDto dto = new CreateWorkerDto(). //
        setName(name);

    final ResponseEntity<WorkerDto> response = this.restTemplate.postForEntity(this.computeUrl(
        WorkerController.POST), dto, WorkerDto.class);

    final WorkerDto worker = response.getBody();
    this.dataCache.registerWorker(name, worker.getId());
  }

  @When("I create worker with name: {word}")
  public void createWorker(String name) {
    final CreateWorkerDto dto = new CreateWorkerDto(). //
        setName(name);

    try {
      final ResponseEntity<WorkerDto> response = this.restTemplate.postForEntity(this.computeUrl(
          WorkerController.POST), dto, WorkerDto.class);

      final WorkerDto worker = response.getBody();
      this.dataCache.registerWorker(name, worker.getId());
    } catch (final Exception e) {
      // Do nothing.
    }
  }

  @Then("There are {int} number of workers in the system")
  public void thenThereAreSpeicificNumberOfWorkers(int numberOfWorkers) {
    final ResponseEntity<WorkerDto[]> response = this.restTemplate.getForEntity(this.computeUrl(
        WorkerController.GET_ALL), WorkerDto[].class);
    assertTrue(response.getStatusCode() == HttpStatus.OK);

    final WorkerDto[] workers = response.getBody();
    assertEquals(numberOfWorkers, workers.length);
  }

  @When("I delete worker: {word}")
  public void whenDeleteWorker(String workerName) {
    final String workerId = this.getWorkerId(workerName);

    try {
      this.restTemplate. //
          delete(this.computeUrl(WorkerController.DELETE), workerId);
    } catch (final Exception e) {
      // Nothing.
    }
  }

}
