/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.bdt;

import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.DefaultResponseErrorHandler;

import com.fasterxml.jackson.databind.ObjectMapper;

import ro.treedev.teamway.error.ApiError;
import ro.treedev.teamway.error.ErrorResponseDto;

@Component
@Profile("test")
public class RestTemplateResponseErrorHandler extends DefaultResponseErrorHandler {

  @Autowired
  private CucumberDataCache cacheService;

  @Autowired
  private ObjectMapper mapper;

  @Override
  public boolean hasError(ClientHttpResponse response) throws IOException {
    final HttpStatus statusCode = response.getStatusCode();

    this.cacheService.setLastResponseStatusCode(statusCode);
    this.cacheService.setLastResponseErrorCodes(null);

    return super.hasError(response);
  }

  @Override
  public void handleError(ClientHttpResponse response) throws IOException {
    try {
      final ErrorResponseDto errorResponse = this.mapper.readValue(this.getResponseBody(response),
          ErrorResponseDto.class);

      final Set<String> errorCodes = errorResponse.getErrors().stream(). //
          map(ApiError::getCode).collect(Collectors.toSet());

      this.cacheService.setLastResponseErrorCodes(errorCodes);
    } finally {
      super.handleError(response);
    }
  }

}
