/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.bdt.steps;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.OffsetDateTime;

import org.springframework.http.ResponseEntity;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import ro.treedev.teamway.api.controller.ShiftController;
import ro.treedev.teamway.api.shift.CreateShiftDto;
import ro.treedev.teamway.api.shift.ShiftDto;
import ro.treedev.teamway.api.worker.WorkerDto;
import ro.treedev.teamway.bdt.SpringIntegrationTest;
import ro.treedev.teamway.dal.worker.WorkerDao;

public class ShiftSteps extends SpringIntegrationTest {

  @When("I create shift for {word} starting at {word} and ending at {word}. Name the shift as: {word}")
  public void createShift(String workerName, String startTime, String endTime, String shiftName) {
    final CreateShiftDto dto = new CreateShiftDto(). //
        setStartTime(OffsetDateTime.parse(startTime)). //
        setEndTime(OffsetDateTime.parse(endTime));

    final String workerId = this.getWorkerId(workerName);

    try {
      final ResponseEntity<ShiftDto> response = this.restTemplate. //
          postForEntity(this.computeUrl(ShiftController.POST), dto, ShiftDto.class, workerId);

      this.dataCache.registerShift(shiftName, response.getBody().getId());
    } catch (final Exception e) {
      // Do nothing.
    }
  }

  @Then("There are {int} shifts in the system")
  public void thenThereAreNumberOfShifts(int numberOfShifts) {
    final WorkerDao[] workers = this.restTemplate. //
        getForObject(this.computeUrl(ShiftController.GET_ALL), WorkerDao[].class);

    assertEquals(numberOfShifts, workers.length);
  }

  @Then("{word} has {int} shifts")
  public void thenWorkerHasNumberOfShifts(String workerName, int numberOfShifts) {
    final String workerId = this.getWorkerId(workerName);

    final WorkerDto[] workers = this.restTemplate. //
        getForObject(this.computeUrl(ShiftController.GET_ALL_BY_WOKRER_ID), WorkerDto[].class,
            workerId);

    assertEquals(numberOfShifts, workers.length);
  }

  @When("I delete shift: {word}")
  public void whenDeleteShift(String shiftName) {
    final String shiftId = this.getShiftId(shiftName);

    try {
      this.restTemplate. //
          delete(this.computeUrl(ShiftController.DELETE), shiftId);
    } catch (final Exception e) {
      // Nothing.
    }
  }

}
