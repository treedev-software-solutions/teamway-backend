/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.bdt;

import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import io.cucumber.messages.internal.com.google.common.collect.Maps;

@Component
@Profile("test")
public class CucumberDataCache {

  private HttpStatus lastResponseStatusCode;

  private Set<String> lastResponseErrorCodes;

  private Map<String, String> workerCache = Maps.newHashMap();

  private Map<String, String> shiftCache = Maps.newHashMap();

  public void setLastResponseStatusCode(HttpStatus statusCode) {
    this.lastResponseStatusCode = statusCode;
  }

  public HttpStatus getLastResponseStatusCode() {
    return this.lastResponseStatusCode;
  }

  public void setLastResponseErrorCodes(Set<String> errorCodes) {
    this.lastResponseErrorCodes = errorCodes;
  }

  public Set<String> getLastResponseErrorCodes() {
    return this.lastResponseErrorCodes;
  }

  public String registerWorker(String name, String id) {
    return this.workerCache.put(name, id);
  }

  public Optional<String> getWorkerId(String name) {
    return Optional.ofNullable(this.workerCache.get(name));
  }

  public String registerShift(String name, String id) {
    return this.shiftCache.put(name, id);
  }

  public Optional<String> getShiftId(String name) {
    return Optional.ofNullable(this.shiftCache.get(name));
  }

  public void clear() {
    this.workerCache.clear();
  }

}
