/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.bdt;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.client.RestTemplate;

@Configuration
@Profile("test")
public class CucumberConfig {

  /**
   * Constructs the rest template which is used by the cucumber steps.
   * 
   * @param builder
   * @param errorHandler
   * @return
   */
  @Bean
  public RestTemplate restTemplate(RestTemplateBuilder builder,
      RestTemplateResponseErrorHandler errorHandler) {
    return builder.errorHandler(errorHandler).build();
  }

}
