/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.bdt;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import ro.treedev.teamway.bll.DatabaseCleanupEvent;

public class CucumberHookHandler {

  @Autowired
  private ApplicationEventPublisher eventPublisher;

  @Autowired
  private CucumberDataCache cacheService;

  @Before
  public void springContextInitialization(Scenario scenario) {
    try {
      // This is required just because the data access layer is configured to use cache while the
      // cache manager is configured for WebApplicationContext.SCOPE_REQUEST scope.
      RequestContextHolder.setRequestAttributes(new FakeRequestAttributes());

      this.eventPublisher.publishEvent(new DatabaseCleanupEvent(this));
      this.cacheService.clear();
    } finally {
      RequestContextHolder.resetRequestAttributes();
    }

  }

  private static final class FakeRequestAttributes implements RequestAttributes {

    private Map<String, Object> requestAttributeMap = new HashMap<>();

    @Override
    public Object getAttribute(String name, int scope) {
      if (scope == RequestAttributes.SCOPE_REQUEST) {
        return this.requestAttributeMap.get(name);
      }
      return null;
    }

    @Override
    public void setAttribute(String name, Object value, int scope) {
      if (scope == RequestAttributes.SCOPE_REQUEST) {
        this.requestAttributeMap.put(name, value);
      }
    }

    @Override
    public void removeAttribute(String name, int scope) {
      if (scope == RequestAttributes.SCOPE_REQUEST) {
        this.requestAttributeMap.remove(name);
      }
    }

    @Override
    public String[] getAttributeNames(int scope) {
      if (scope == RequestAttributes.SCOPE_REQUEST) {
        return this.requestAttributeMap.keySet().toArray(new String[0]);
      }
      return new String[0];
    }

    @Override
    public void registerDestructionCallback(String name, Runnable callback, int scope) {
      // Not Supported
    }

    @Override
    public Object resolveReference(String key) {
      // Not supported
      return null;
    }

    @Override
    public String getSessionId() {
      return null;
    }

    @Override
    public Object getSessionMutex() {
      return null;
    }
  }

}
