/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.bdt;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/bdt/features")
public class CucumberIntegrationTest {
  // The tests will be picked up automatically.
}
