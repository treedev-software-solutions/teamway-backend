/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.bdt.steps;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.util.Set;

import org.springframework.http.HttpStatus;

import io.cucumber.java.en.Then;
import ro.treedev.teamway.bdt.SpringIntegrationTest;

public class GenericSteps extends SpringIntegrationTest {

  @Then("The last request has been succeeded")
  public void theLastRequestSucceeded() {
    final HttpStatus lastResponseStatusCode = this.dataCache.getLastResponseStatusCode();
    final Set<String> lastResponseErrorCodes = this.dataCache.getLastResponseErrorCodes();

    assertNotNull(lastResponseStatusCode);
    assertEquals(HttpStatus.OK, lastResponseStatusCode, "Last response error codes: "
        + lastResponseErrorCodes);
  }

  @Then("The last request has been failed")
  public void theLastRequestFailed() {
    final HttpStatus lastResponseStatusCode = this.dataCache.getLastResponseStatusCode();

    assertNotNull(lastResponseStatusCode);
    assertNotEquals(HttpStatus.OK, lastResponseStatusCode);
  }

  @Then("The last request has been failed with {word} error code")
  public void theLastRequestFailed(String errorCode) {
    final Set<String> lastResponseErrorCodes = this.dataCache.getLastResponseErrorCodes();

    assertNotNull(lastResponseErrorCodes);
    assertTrue("Last response error codes: " + lastResponseErrorCodes,
        lastResponseErrorCodes.contains(errorCode));
  }
}
