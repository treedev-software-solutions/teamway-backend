/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.bll.shift;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import org.junit.Test;

public class ShiftTest {

  private static final ZoneOffset DEFAULT_OFFSET = ZoneOffset.UTC;

  private static final LocalDate REFERENCE_DATE = LocalDate.of(2010, 5, 10);

  private static final OffsetDateTime REFERENCE_DATE_TIME = OffsetDateTime.of(REFERENCE_DATE,
      LocalTime.of(10, 0), DEFAULT_OFFSET);

  @Test
  public void getAffectedDatesSameDay() {
    final Shift shift = new Shift(). //
        setStartTime(REFERENCE_DATE_TIME). //
        setEndTime(REFERENCE_DATE_TIME.plusHours(1));

    assertEquals(1, shift.getAffectedDays(DEFAULT_OFFSET).size());
  }

  @Test
  public void getAffectedDatesOneDayBetween() {
    final Shift shift = new Shift(). //
        setStartTime(REFERENCE_DATE_TIME). //
        setEndTime(REFERENCE_DATE_TIME.plusDays(1));

    assertEquals(2, shift.getAffectedDays(DEFAULT_OFFSET).size());
  }

  @Test
  public void getAffectedDatesMultipleDaysBetween() {
    final Shift shift = new Shift(). //
        setStartTime(REFERENCE_DATE_TIME). //
        setEndTime(REFERENCE_DATE_TIME.plusDays(2));

    assertEquals(3, shift.getAffectedDays(DEFAULT_OFFSET).size());
  }

  @Test
  public void intersectsSameShift() {
    final Shift shift = new Shift(). //
        setStartTime(REFERENCE_DATE_TIME). //
        setEndTime(REFERENCE_DATE_TIME.plusHours(8));

    final Shift shift2 = new Shift(). //
        setStartTime(REFERENCE_DATE_TIME). //
        setEndTime(REFERENCE_DATE_TIME.plusHours(8));

    assertTrue(shift.intersects(shift2));
  }

  @Test
  public void intersectsOneAfterOther() {
    final Shift shift = new Shift(). //
        setStartTime(REFERENCE_DATE_TIME). //
        setEndTime(REFERENCE_DATE_TIME.plusHours(8));

    final Shift shift2 = new Shift(). //
        setStartTime(REFERENCE_DATE_TIME.minusHours(8)). //
        setEndTime(REFERENCE_DATE_TIME);

    assertFalse(shift.intersects(shift2));
  }

  @Test
  public void intersectsOneIntoAnother() {
    final Shift shift = new Shift(). //
        setStartTime(REFERENCE_DATE_TIME). //
        setEndTime(REFERENCE_DATE_TIME.plusHours(8));

    final Shift shift2 = new Shift(). //
        setStartTime(REFERENCE_DATE_TIME.plusHours(1)). //
        setEndTime(REFERENCE_DATE_TIME.plusHours(3));

    assertTrue(shift.intersects(shift2));
  }

  @Test
  public void intersectsSameStartDate() {
    final Shift shift = new Shift(). //
        setStartTime(REFERENCE_DATE_TIME). //
        setEndTime(REFERENCE_DATE_TIME.plusHours(8));

    final Shift shift2 = new Shift(). //
        setStartTime(REFERENCE_DATE_TIME). //
        setEndTime(REFERENCE_DATE_TIME.plusHours(3));

    assertTrue(shift.intersects(shift2));
  }

  @Test
  public void containsDateOneDayShift() {
    final Shift shift = new Shift(). //
        setStartTime(REFERENCE_DATE_TIME). //
        setEndTime(REFERENCE_DATE_TIME.plusHours(8));

    assertTrue(shift.contains(REFERENCE_DATE, DEFAULT_OFFSET));
  }

  @Test
  public void notContainsDateOneDayShift() {
    final Shift shift = new Shift(). //
        setStartTime(REFERENCE_DATE_TIME). //
        setEndTime(REFERENCE_DATE_TIME.plusHours(8));

    assertFalse(shift.contains(REFERENCE_DATE.plusDays(1), DEFAULT_OFFSET));
  }

  @Test
  public void containsDateTwoDayShiftAtStart() {
    final Shift shift = new Shift(). //
        setStartTime(REFERENCE_DATE_TIME). //
        setEndTime(REFERENCE_DATE_TIME.plusDays(1));

    assertTrue(shift.contains(REFERENCE_DATE, DEFAULT_OFFSET));
  }

  @Test
  public void containsDateTwoDayShiftAtEnd() {
    final Shift shift = new Shift(). //
        setStartTime(REFERENCE_DATE_TIME). //
        setEndTime(REFERENCE_DATE_TIME.plusDays(1));

    assertTrue(shift.contains(REFERENCE_DATE.plusDays(1), DEFAULT_OFFSET));
  }

  @Test
  public void notContainsDateTwoDayShift() {
    final Shift shift = new Shift(). //
        setStartTime(REFERENCE_DATE_TIME). //
        setEndTime(REFERENCE_DATE_TIME.plusDays(1));

    assertFalse(shift.contains(REFERENCE_DATE.plusDays(2), DEFAULT_OFFSET));
  }

  @Test
  public void containsDateMultipleDayShiftBetween() {
    final Shift shift = new Shift(). //
        setStartTime(REFERENCE_DATE_TIME). //
        setEndTime(REFERENCE_DATE_TIME.plusDays(2));

    assertTrue(shift.contains(REFERENCE_DATE.plusDays(1), DEFAULT_OFFSET));
  }

  @Test
  public void intersectedDaysSameShifts() {
    final Shift shift = new Shift(). //
        setStartTime(REFERENCE_DATE_TIME). //
        setEndTime(REFERENCE_DATE_TIME.plusHours(8));

    final Shift shift2 = new Shift(). //
        setStartTime(REFERENCE_DATE_TIME). //
        setEndTime(REFERENCE_DATE_TIME.plusHours(8));

    assertEquals(1, shift.intersectedDays(shift2, DEFAULT_OFFSET).size());
  }

  @Test
  public void intersectedDaysSameShiftsTwoDays() {
    final Shift shift = new Shift(). //
        setStartTime(REFERENCE_DATE_TIME). //
        setEndTime(REFERENCE_DATE_TIME.plusDays(1));

    final Shift shift2 = new Shift(). //
        setStartTime(REFERENCE_DATE_TIME). //
        setEndTime(REFERENCE_DATE_TIME.plusDays(1));

    assertEquals(2, shift.intersectedDays(shift2, DEFAULT_OFFSET).size());
  }

  @Test
  public void intersectedDaysSameShiftsMultipleDays() {
    final Shift shift = new Shift(). //
        setStartTime(REFERENCE_DATE_TIME). //
        setEndTime(REFERENCE_DATE_TIME.plusDays(3));

    final Shift shift2 = new Shift(). //
        setStartTime(REFERENCE_DATE_TIME). //
        setEndTime(REFERENCE_DATE_TIME.plusDays(3));

    assertEquals(4, shift.intersectedDays(shift2, DEFAULT_OFFSET).size());
  }

  @Test
  public void intersectedDaysOneDayIntersection() {
    final Shift shift = new Shift(). //
        setStartTime(REFERENCE_DATE_TIME.minusDays(1)). //
        setEndTime(REFERENCE_DATE_TIME);

    final Shift shift2 = new Shift(). //
        setStartTime(REFERENCE_DATE_TIME). //
        setEndTime(REFERENCE_DATE_TIME.plusDays(3));

    assertEquals(1, shift.intersectedDays(shift2, DEFAULT_OFFSET).size());
  }

  @Test
  public void intersectedDaysTwoDaysIntersection() {
    final Shift shift = new Shift(). //
        setStartTime(REFERENCE_DATE_TIME.minusDays(10)). //
        setEndTime(REFERENCE_DATE_TIME.plusDays(1));

    final Shift shift2 = new Shift(). //
        setStartTime(REFERENCE_DATE_TIME). //
        setEndTime(REFERENCE_DATE_TIME.plusDays(10));

    assertEquals(2, shift.intersectedDays(shift2, DEFAULT_OFFSET).size());
  }

}
