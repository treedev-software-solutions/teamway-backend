Feature: Worker

Scenario: Successfully create a worker
	When I create worker with name: Eva
	Then There are 1 number of workers in the system
	
Scenario: Successfully create three worker
	When I create worker with name: Eva
	When I create worker with name: John
	When I create worker with name: Arnold
	Then There are 3 number of workers in the system
	
Scenario: Delete worker
	When I create worker with name: Eva
	When I create worker with name: John
	When I create worker with name: Arnold
	Then There are 3 number of workers in the system
	When I delete worker: Eva
	Then There are 2 number of workers in the system
	When I delete worker: John
	Then There are 1 number of workers in the system
	When I delete worker: Arnold
	Then There are 0 number of workers in the system
	
Scenario: Delete worker
	When I create worker with name: Eva
	When I create worker with name: Arnold
	Then There are 2 number of workers in the system
	When I create shift for Eva starting at 2021-02-05T00:00:00.000000+03:00 and ending at 2021-02-05T08:00:00.000000+03:00. Name the shift as: Shift1
	When I create shift for Arnold starting at 2021-02-05T00:00:00.000000+03:00 and ending at 2021-02-05T08:00:00.000000+03:00. Name the shift as: Shift1
	Then Eva has 1 shifts
	Then Arnold has 1 shifts
	Then There are 2 shifts in the system
	When I delete worker: Eva
	Then There are 1 shifts in the system
	When I delete worker: Arnold
	Then There are 0 shifts in the system