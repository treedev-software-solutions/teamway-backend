Feature: Shift

Background:
	Given a worker with name: Eva
	Given a worker with name: Arnold

Scenario: Successfully create a shift
	When I create shift for Eva starting at 2021-02-05T00:00:00.000000+03:00 and ending at 2021-02-05T08:00:00.000000+03:00. Name the shift as: Shift1
	Then The last request has been succeeded
	
Scenario: Two similar shifts
	When I create shift for Eva starting at 2021-02-05T00:00:00.000000+03:00 and ending at 2021-02-05T08:00:00.000000+03:00. Name the shift as: Shift1
	Then The last request has been succeeded
	When I create shift for Eva starting at 2021-02-05T00:00:00.000000+03:00 and ending at 2021-02-05T08:00:00.000000+03:00. Name the shift as: Shift2
	Then The last request has been failed with INTERSECTING_SHIFT error code
	
Scenario: Two shifts at the same day
	When I create shift for Eva starting at 2021-02-05T00:00:00.000000+03:00 and ending at 2021-02-05T08:00:00.000000+03:00. Name the shift as: Shift1
	Then The last request has been succeeded
	When I create shift for Eva starting at 2021-02-05T08:00:00.000000+03:00 and ending at 2021-02-05T16:00:00.000000+03:00. Name the shift as: Shift2
	Then The last request has been failed with INVALID_NUMBER_OF_SHIFTS_AT_THE_SAME_DAY error code
	
Scenario: Two shifts at different days
	When I create shift for Eva starting at 2021-02-05T00:00:00.000000+03:00 and ending at 2021-02-05T08:00:00.000000+03:00. Name the shift as: Shift1
	Then The last request has been succeeded
	When I create shift for Eva starting at 2021-02-06T08:00:00.000000+03:00 and ending at 2021-02-06T16:00:00.000000+03:00. Name the shift as: Shift2
	Then The last request has been succeeded
	
Scenario: The shift is not 8h long
	When I create shift for Eva starting at 2021-02-05T00:00:00.000000+03:00 and ending at 2021-02-05T05:00:00.000000+03:00. Name the shift as: Shift1
	Then The last request has been failed with INVALID_REQUEST error code
	
Scenario: Invalid shift interval
	When I create shift for Eva starting at 2021-02-05T08:00:00.000000+03:00 and ending at 2021-02-05T07:00:00.000000+03:00. Name the shift as: Shift1
	Then The last request has been failed with INVALID_REQUEST error code
	
Scenario: Invalid shift start time
	When I create shift for Eva starting at 2021-02-05T01:00:00.000000+03:00 and ending at 2021-02-05T09:00:00.000000+03:00. Name the shift as: Shift1
	Then The last request has been failed with INVALID_REQUEST error code
	
Scenario: Too long shift
	When I create shift for Eva starting at 2021-02-05T00:00:00.000000+03:00 and ending at 2021-02-05T21:00:00.000000+03:00. Name the shift as: Shift1
	Then The last request has been failed with INVALID_REQUEST error code
	
Scenario: Same shift for two users
	When I create shift for Eva starting at 2021-02-05T00:00:00.000000+03:00 and ending at 2021-02-05T08:00:00.000000+03:00. Name the shift as: Shift1
	Then The last request has been succeeded
	Then Eva has 1 shifts
	When I create shift for Arnold starting at 2021-02-05T00:00:00.000000+03:00 and ending at 2021-02-05T08:00:00.000000+03:00. Name the shift as: Shift2
	Then The last request has been succeeded
	Then Arnold has 1 shifts
	Then There are 2 shifts in the system

Scenario: Delete shifts
	When I create shift for Eva starting at 2021-02-05T00:00:00.000000+03:00 and ending at 2021-02-05T08:00:00.000000+03:00. Name the shift as: Shift1
	Then The last request has been succeeded
	Then Eva has 1 shifts
	When I create shift for Eva starting at 2021-02-06T08:00:00.000000+03:00 and ending at 2021-02-06T16:00:00.000000+03:00. Name the shift as: Shift2
	Then The last request has been succeeded
	Then Eva has 2 shifts
	When I delete shift: Shift2
	Then The last request has been succeeded
	Then Eva has 1 shifts
	When I delete shift: Shift1
	Then The last request has been succeeded
	Then Eva has 0 shifts
