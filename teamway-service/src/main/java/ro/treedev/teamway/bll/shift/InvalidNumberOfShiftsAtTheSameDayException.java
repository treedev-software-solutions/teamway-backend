/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.bll.shift;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;
import ro.treedev.teamway.core.error.ValuedRuntimeException;

@Data
@ToString(callSuper = true)
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class InvalidNumberOfShiftsAtTheSameDayException extends ValuedRuntimeException {

  /** serialVersionUID */
  private static final long serialVersionUID = 1L;

  public InvalidNumberOfShiftsAtTheSameDayException(String value) {
    super(
        "The system does not allow the operation, because the number of schedules at a day would not be valid.",
        value);
  }

}
