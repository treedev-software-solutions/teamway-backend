/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.bll;

import javax.json.JsonPatch;
import javax.json.JsonStructure;
import javax.json.JsonValue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class DefaultJSonPatchService implements JSonPatchService {

  @Autowired
  private ObjectMapper objectMapper;

  @Autowired
  private ValidatorService validator;

  @Override
  public <T> T validatedPatch(JsonPatch patch, T targetBean, Class<T> beanClass) {
    final T ret = this.patch(patch, targetBean, beanClass);
    this.validator.validate(ret);

    return ret;
  }

  <T> T patch(JsonPatch patch, T targetBean, Class<T> beanClass) {
    // Convert the Java bean to a JSON document
    final JsonStructure target = this.objectMapper.convertValue(targetBean, JsonStructure.class);

    // Apply the JSON Patch to the JSON document
    final JsonValue patched = patch.apply(target);

    // Convert the JSON document to a Java bean
    final T beanPatched = this.objectMapper.convertValue(patched, beanClass);

    // Return the bean that has been patched
    return beanPatched;
  }

}
