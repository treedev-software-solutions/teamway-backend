/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.bll.worker;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.NullValuePropertyMappingStrategy;

import ro.treedev.teamway.api.worker.WorkerDto;
import ro.treedev.teamway.dal.worker.WorkerDao;

@Mapper(componentModel = "spring", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS, nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_NULL)
public interface WorkerMapper {

  Worker toDomain(WorkerDao e);

  WorkerDao toDao(Worker e);

  Worker toDomain(WorkerDto e);

  WorkerDto toDto(Worker e);

  public PatchWorker toPatch(Worker domain);

  public Worker patch(@MappingTarget Worker target, PatchWorker source);

}
