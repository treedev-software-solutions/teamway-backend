/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.bll;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Default {@link ValidatorService} implementation.
 * 
 * @author Arnold Robert Turdean
 */
@Service
public class DefaultValidatorService implements ValidatorService {

  @Autowired
  private Validator validator;

  /**
   * @see ro.treedev.teamway.bll.ValidatorService#validate(java.lang.Object)
   */
  @Override
  public <T> void validate(T entity) throws ConstraintViolationException, ValidationException {
    this.validateByGroups(entity);
  }

  /**
   * @see ro.treedev.teamway.bll.ValidatorService#validateByGroups(java.lang.Object,
   *      java.lang.Class[])
   */
  @Override
  public <T> void validateByGroups(T entity, Class<?>... groups)
      throws ConstraintViolationException, ValidationException {
    final Set<ConstraintViolation<T>> violations = this.validator.validate(entity, groups);
    if (violations != null && !violations.isEmpty()) {
      throw new ConstraintViolationException(violations);
    }
  }

}
