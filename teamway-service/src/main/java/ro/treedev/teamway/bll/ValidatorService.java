/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.bll;

import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;

/**
 * Validator service.
 * 
 * @author Arnold Robert Turdean
 */
public interface ValidatorService {

  /**
   * Validate the given entity instance.
   * 
   * @param <T>
   * 
   * @param entity
   * 
   * @throws ConstraintViolationException
   * @throws ValidationException
   */
  <T> void validate(T entity) throws ConstraintViolationException, ValidationException;

  /**
   * Validate the given entity instance with the given validator groups.
   *
   * @param <T>
   *
   * @param entity
   * @param groups
   *
   * @throws ConstraintViolationException
   * @throws ValidationException
   */
  <T> void validateByGroups(T entity, Class<?>... groups) throws ConstraintViolationException,
      ValidationException;

}
