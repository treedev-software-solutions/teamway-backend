/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.bll;

import org.springframework.context.ApplicationEvent;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * The event is fired when the application all the stored data has to be cleared.
 * 
 * @author Arnold Robert Turdean
 */
@Data
@ToString(callSuper = true)
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class DatabaseCleanupEvent extends ApplicationEvent {

  /** serialVersionUID */
  private static final long serialVersionUID = 1L;

  /**
   * Constructs a new instance.
   * 
   * @param source
   */
  public DatabaseCleanupEvent(Object source) {
    super(source);
  }
}
