/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.bll.shift;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.ZoneOffset;
import java.util.List;

import javax.validation.Constraint;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.Lists;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import ro.treedev.teamway.AppConfig;
import ro.treedev.teamway.bll.shift.Shift.ValidShift;
import ro.treedev.teamway.core.validator.AbstractConstraintValidator;

@Data
@ToString(callSuper = false)
@Accessors(chain = true)
@ValidShift
public class Shift {

  private String id;

  @NotNull
  @Size(min = 1)
  private String workerId;

  @NotNull
  private OffsetDateTime startTime;

  @NotNull
  private OffsetDateTime endTime;

  public Duration getDuration() {
    return Duration.between(this.startTime, this.endTime);
  }

  public boolean intersects(Shift shift) {
    if (this.endTime.equals(shift.getStartTime()) || this.startTime.equals(shift.getEndTime())) {
      return false;
    }

    return this.contains(shift.getStartTime()) || this.contains(shift.getEndTime());
  }

  private boolean contains(OffsetDateTime time) {
    return !time.isBefore(this.startTime) && !time.isAfter(this.endTime);
  }

  public OffsetDateTime getStartTime(ZoneOffset offset) {
    return this.startTime.withOffsetSameInstant(offset);
  }

  public OffsetDateTime getEndTime(ZoneOffset offset) {
    return this.endTime.withOffsetSameInstant(offset);
  }

  public List<LocalDate> getAffectedDays(ZoneOffset offset) {
    final LocalDate startDate = this.getStartTime(offset).toLocalDate();
    final LocalDate endDate = this.getEndTime(offset).toLocalDate();

    final List<LocalDate> ret = Lists.newArrayList();

    LocalDate currentDate = startDate;
    do {
      ret.add(currentDate);
      currentDate = currentDate.plusDays(1);
    } while (!currentDate.isAfter(endDate));

    return ret;
  }

  public List<LocalDate> intersectedDays(Shift shift, ZoneOffset offset) {
    final LocalDate startDate = this.getStartTime(offset).toLocalDate();
    final LocalDate endDate = this.getEndTime(offset).toLocalDate();

    final List<LocalDate> ret = Lists.newArrayList();

    LocalDate currentDate = shift.getStartTime(offset).toLocalDate();
    final LocalDate shiftEndDate = shift.getEndTime(offset).toLocalDate();
    do {
      if (shift.contains(startDate, endDate, currentDate)) {
        ret.add(currentDate);
      }

      currentDate = currentDate.plusDays(1);
    } while (!currentDate.isAfter(shiftEndDate));

    return ret;
  }

  public boolean contains(LocalDate date, ZoneOffset offset) {
    final LocalDate startDate = this.getStartTime(offset).toLocalDate();
    final LocalDate endDate = this.getEndTime(offset).toLocalDate();

    return this.contains(startDate, endDate, date);
  }

  private boolean contains(LocalDate startDate, LocalDate endDate, LocalDate date) {
    return startDate.equals(date) || endDate.equals(date) || startDate.isBefore(date)
        && endDate.isAfter(date);
  }

  /**
   * Validates an {@link Shift}.
   * 
   * @author Arnold Robert Turdean
   */
  @Documented
  @Target({ ElementType.TYPE })
  @Retention(RetentionPolicy.RUNTIME)
  @Constraint(validatedBy = { ValidShift.Validator.class })
  public @interface ValidShift {

    /**
     * Get message.
     * 
     * @return
     */
    String message() default "Invalid Shift";

    /**
     * Get groups.
     * 
     * @return
     */
    Class<?>[] groups() default {};

    /**
     * Get payload.
     * 
     * @return
     */
    Class<? extends Payload>[] payload() default {};

    /**
     * Validator.
     * 
     * @author Arnold Robert Turdean
     */
    public static class Validator extends AbstractConstraintValidator<ValidShift, Shift> {

      @Autowired
      private AppConfig appConfig;

      @Autowired
      private ShiftConfig shiftConfig;

      /**
       * @see ro.treedev.commons.validator.AbstractConstraintValidator#validate(java.lang.Object,
       *      javax.validation.ConstraintValidatorContext)
       */
      @Override
      public boolean validate(Shift shift, ConstraintValidatorContext context) {
        if (shift == null) {
          return this.addValueShouldBeMentionedError(context);
        }

        boolean ret = true;
        final OffsetDateTime startDateTime = shift.getStartTime();
        if (!startDateTime.isBefore(shift.getEndTime())) {
          ret &= this.addError("The start time must be before the end time", context);
        }

        final Duration duration = shift.getDuration();
        final Duration minShiftDuration = this.shiftConfig.getMinShiftDuration();
        if (duration.compareTo(minShiftDuration) < 0) {
          ret &= this.addError("The shift's duration must be minimum " + minShiftDuration, context);
        }

        final Duration maxShiftDuration = this.shiftConfig.getMaxShiftDuration();
        if (duration.compareTo(maxShiftDuration) > 0) {
          ret &= this.addError("The shift's duration must be maximum " + maxShiftDuration, context);
        }

        final List<LocalTime> validShiftStartTimes = this.shiftConfig.getValidShiftStartTimes();
        if (validShiftStartTimes != null) {
          final ZoneOffset appZoneOffset = this.appConfig.getZoneOffset();

          final OffsetTime startTime = startDateTime.toOffsetTime();
          if (!validShiftStartTimes.stream().anyMatch(t -> this.isAllowedStartTime(startTime, t,
              appZoneOffset))) {
            ret &= this.addError("The shift's start time is not allowed by the system", context);
          }
        }

        return ret;
      }

      private boolean isAllowedStartTime(OffsetTime startTime, LocalTime allowedLocalTime,
          ZoneOffset appZoneOffset) {
        final OffsetTime normalizedStartTime = startTime.withOffsetSameInstant(appZoneOffset);
        final OffsetTime normalizedAllowedTime = allowedLocalTime.atOffset(appZoneOffset);

        return normalizedAllowedTime.equals(normalizedStartTime);
      }

    }
  }

}
