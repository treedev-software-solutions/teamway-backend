/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.bll.shift;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;
import ro.treedev.teamway.core.error.ValuedRuntimeException;

@Data
@ToString(callSuper = true)
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class UnknownShiftException extends ValuedRuntimeException {

  /** serialVersionUID */
  private static final long serialVersionUID = 1L;

  public UnknownShiftException(String value) {
    super("The shift was not found.", value);
  }

}
