/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.bll.worker;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString(callSuper = false)
@Accessors(chain = true)
public class Worker {

  private String id;

  @NotNull
  @Size(min = 1)
  private String name;

}
