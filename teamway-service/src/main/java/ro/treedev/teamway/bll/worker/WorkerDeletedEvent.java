/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.bll.worker;

import org.springframework.context.ApplicationEvent;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * The event is fired when a worker has been removed from the system.
 * 
 * @author Arnold Robert Turdean
 */
@Data
@ToString(callSuper = true)
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class WorkerDeletedEvent extends ApplicationEvent {

  /** serialVersionUID */
  private static final long serialVersionUID = 1L;

  private final String workerId;

  /**
   * Constructs a new instance.
   * 
   * @param source
   * @param workerId
   */
  public WorkerDeletedEvent(Object source, String workerId) {
    super(source);

    this.workerId = workerId;
  }
}
