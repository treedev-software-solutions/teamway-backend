/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.bll.shift;

import java.time.Duration;
import java.time.LocalTime;
import java.util.List;
import java.util.Set;

import javax.validation.constraints.Min;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@Configuration
@ConfigurationProperties(prefix = "app.shift")
@Validated
public class ShiftConfig {

  private Duration minShiftDuration = Duration.ofHours(8);

  private Duration maxShiftDuration = Duration.ofHours(8);

  private Set<@Min(2) Integer> notAllowedNumberOfShiftsAtTheSameDay = Sets.newHashSet(2);

  private List<LocalTime> validShiftStartTimes = Lists.newArrayList(LocalTime.of(0, 0),
      LocalTime.of(8, 0), LocalTime.of(16, 0));

}
