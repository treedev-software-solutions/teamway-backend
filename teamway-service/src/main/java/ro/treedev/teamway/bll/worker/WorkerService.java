/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.bll.worker;

import java.util.Optional;
import java.util.stream.Stream;

import javax.json.JsonPatch;

import ro.treedev.teamway.bll.DatabaseCleanupEventListener;

public interface WorkerService extends DatabaseCleanupEventListener {

  Worker create(String name);

  Optional<Worker> findById(String id);

  Worker getById(String id);

  Worker patch(String id, JsonPatch patch);

  Stream<Worker> getAll();
  
  Worker save(Worker entity);
  
  void delete(String workerId);

}
