/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.bll.shift;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.NullValuePropertyMappingStrategy;

import ro.treedev.teamway.api.shift.ShiftDto;
import ro.treedev.teamway.dal.shift.ShiftDao;

@Mapper(componentModel = "spring", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS, nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_NULL)
public interface ShiftMapper {

  Shift toDomain(ShiftDao e);

  ShiftDao toDao(Shift e);

  Shift toDomain(ShiftDto e);

  ShiftDto toDto(Shift e);

  public PatchShift toPatch(Shift domain);

  public Shift patch(@MappingTarget Shift target, PatchShift source);

}
