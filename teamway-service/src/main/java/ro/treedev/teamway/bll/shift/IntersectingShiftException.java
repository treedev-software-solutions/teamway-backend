/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.bll.shift;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;
import ro.treedev.teamway.core.error.ValuedRuntimeException;

@Data
@ToString(callSuper = true)
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class IntersectingShiftException extends ValuedRuntimeException {

  /** serialVersionUID */
  private static final long serialVersionUID = 1L;

  public IntersectingShiftException(String value) {
    super("The shift is intersecting with an existing one.", value);
  }

}
