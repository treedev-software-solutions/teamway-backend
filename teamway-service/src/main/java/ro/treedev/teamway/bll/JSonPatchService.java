/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.bll;

import javax.json.JsonPatch;
import javax.validation.ConstraintViolationException;

public interface JSonPatchService {

  /**
   * Patch the given target bean with the given {@link JsonPatch}. The method throws
   * {@link ConstraintViolationException} if the validation fails.
   * 
   * @param <T>
   * @param patch
   * @param targetBean
   * @param beanClass
   * @return
   */
  <T> T validatedPatch(JsonPatch patch, T targetBean, Class<T> beanClass);

}
