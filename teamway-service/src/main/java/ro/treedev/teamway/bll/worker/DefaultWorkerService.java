/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.bll.worker;

import java.util.Optional;
import java.util.stream.Stream;

import javax.json.JsonPatch;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

import lombok.extern.log4j.Log4j2;
import ro.treedev.teamway.bll.DatabaseCleanupEvent;
import ro.treedev.teamway.bll.InvalidJsonPatchException;
import ro.treedev.teamway.bll.JSonPatchService;
import ro.treedev.teamway.bll.ValidatorService;
import ro.treedev.teamway.dal.worker.WorkerDao;
import ro.treedev.teamway.dal.worker.WorkerRepository;

@Log4j2
@Service
public class DefaultWorkerService implements WorkerService {

  @Autowired
  private WorkerMapper mapper;

  @Autowired
  private JSonPatchService jsonPatchService;

  @Autowired
  private WorkerRepository repository;

  @Autowired
  private ValidatorService validator;

  @Autowired
  private ApplicationEventPublisher eventPublisher;

  @Override
  public Worker create(String name) {
    Preconditions.checkNotNull(name);

    final Worker worker = new Worker(). //
        setName(name);

    final Worker ret = this.save(worker);
    log.info("{} worker has been created.", ret.getId());

    return ret;
  }

  /**
   * @see ro.treedev.teamway.bll.worker.WorkerService#findById(java.lang.String)
   */
  @Override
  public Optional<Worker> findById(String id) {
    if (!Strings.isNullOrEmpty(id)) {
      return this.repository.findById(id).map(u -> this.mapper.toDomain(u));
    }

    return Optional.empty();
  }

  /**
   * @see ro.treedev.teamway.bll.worker.WorkerService#getById(java.lang.String)
   */
  @Override
  public Worker getById(String id) {
    Preconditions.checkNotNull(id);

    return this.findById(id).orElseThrow(() -> new UnknownWorkerException(id));
  }

  /**
   * @see ro.treedev.teamway.bll.worker.WorkerService#getAll()
   */
  @Override
  public Stream<Worker> getAll() {
    return this.repository.getAll().map(this.mapper::toDomain);
  }

  /**
   * @see ro.treedev.teamway.bll.worker.WorkerService#patch(java.lang.String, javax.json.JsonPatch)
   */
  @Override
  public Worker patch(String id, JsonPatch patch) {
    Preconditions.checkNotNull(id);

    final Worker worker = this.getById(id);

    try {
      final PatchWorker patchableData = this.mapper.toPatch(worker);
      final PatchWorker patchedData = this.jsonPatchService.validatedPatch(patch, patchableData,
          PatchWorker.class);
      this.mapper.patch(worker, patchedData);
    } catch (final Exception e) {
      throw InvalidJsonPatchException.of(e);
    }

    final Worker ret = this.save(worker);
    log.info("{} worker has been updated", ret.getId());

    return ret;
  }

  /**
   * @see ro.treedev.teamway.bll.worker.WorkerService#save(ro.treedev.teamway.bll.worker.Worker)
   */
  @Override
  public Worker save(Worker entity) {
    this.validator.validate(entity);

    final WorkerDao dao = this.mapper.toDao(entity);
    final WorkerDao saved = this.repository.save(dao);
    return this.mapper.toDomain(saved);
  }

  /**
   * @see ro.treedev.teamway.bll.worker.WorkerService#delete(java.lang.String)
   */
  @Override
  public void delete(String workerId) {
    Preconditions.checkNotNull(workerId);

    this.repository.deleteById(workerId);
    this.eventPublisher.publishEvent(new WorkerDeletedEvent(this, workerId));
  }

  /**
   * @see ro.treedev.teamway.bll.DatabaseCleanupEventListener#handle(ro.treedev.teamway.bll.DatabaseCleanupEvent)
   */
  @Override
  public void handle(DatabaseCleanupEvent event) {
    this.repository.deleteAll();
  }

}
