/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.bll.shift;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.json.JsonPatch;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;

import lombok.extern.log4j.Log4j2;
import ro.treedev.teamway.AppConfig;
import ro.treedev.teamway.bll.DatabaseCleanupEvent;
import ro.treedev.teamway.bll.InvalidJsonPatchException;
import ro.treedev.teamway.bll.JSonPatchService;
import ro.treedev.teamway.bll.ValidatorService;
import ro.treedev.teamway.bll.worker.UnknownWorkerException;
import ro.treedev.teamway.bll.worker.Worker;
import ro.treedev.teamway.bll.worker.WorkerDeletedEvent;
import ro.treedev.teamway.bll.worker.WorkerService;
import ro.treedev.teamway.dal.shift.ShiftDao;
import ro.treedev.teamway.dal.shift.ShiftRepository;

@Log4j2
@Service
public class DefaultShiftService implements ShiftService {

  @Autowired
  private ValidatorService validator;

  @Autowired
  private WorkerService workerService;

  @Autowired
  private ShiftRepository repository;

  @Autowired
  private ShiftMapper mapper;

  @Autowired
  private JSonPatchService jsonPatchService;

  @Autowired
  private AppConfig appConfig;

  @Autowired
  private ShiftConfig shiftConfig;

  @Override
  public Shift create(String workerId, OffsetDateTime startTime, OffsetDateTime endTime) {
    Preconditions.checkNotNull(workerId);
    Preconditions.checkNotNull(startTime);
    Preconditions.checkNotNull(endTime);

    final Worker worker = this.workerService.getById(workerId);

    final Shift shift = new Shift(). //
        setWorkerId(workerId). //
        setStartTime(startTime). //
        setEndTime(endTime);

    final Shift ret = this.save(shift);
    log.info("A new shift has been created for {}", worker.getId());

    return ret;
  }

  /**
   * @see ro.treedev.teamway.bll.shift.ShiftService#findById(java.lang.String)
   */
  @Override
  public Optional<Shift> findById(String id) {
    if (!Strings.isNullOrEmpty(id)) {
      return this.repository.findById(id).map(u -> this.mapper.toDomain(u));
    }

    return Optional.empty();
  }

  /**
   * @see ro.treedev.teamway.bll.shift.ShiftService#getById(java.lang.String)
   */
  @Override
  public Shift getById(String id) {
    Preconditions.checkNotNull(id);

    return this.findById(id).orElseThrow(() -> new UnknownWorkerException(id));
  }

  /**
   * @see ro.treedev.teamway.bll.shift.ShiftService#patch(java.lang.String, javax.json.JsonPatch)
   */
  @Override
  public Shift patch(String id, JsonPatch patch) {
    Preconditions.checkNotNull(id);

    final Shift shift = this.getById(id);

    try {
      final PatchShift patchableData = this.mapper.toPatch(shift);
      final PatchShift patchedData = this.jsonPatchService.validatedPatch(patch, patchableData,
          PatchShift.class);
      this.mapper.patch(shift, patchedData);
    } catch (final Exception e) {
      throw InvalidJsonPatchException.of(e);
    }

    return this.save(shift);
  }

  /**
   * @see ro.treedev.teamway.bll.shift.ShiftService#getAll(java.lang.String)
   */
  @Override
  public Stream<Shift> getAll(String workerId) {
    Preconditions.checkNotNull(workerId);

    return this.repository.findAllByWorkerId(workerId).map(this.mapper::toDomain);
  }

  /**
   * @see ro.treedev.teamway.bll.shift.ShiftService#getAll()
   */
  @Override
  public Stream<Shift> getAll() {
    return this.repository.getAll().map(this.mapper::toDomain);
  }

  @Override
  @Transactional
  public Shift save(Shift entity) {
    this.validator.validate(entity);

    final ZoneOffset offset = this.appConfig.getZoneOffset();

    final OffsetDateTime from = entity.getStartTime(). //
        withOffsetSameInstant(offset).toLocalDate().atStartOfDay().atOffset(offset);
    final OffsetDateTime to = entity.getEndTime(). //
        withOffsetSameInstant(offset).toLocalDate().atStartOfDay().plusDays(1).atOffset(offset);

    final List<Shift> shifts = this.repository. //
        getShiftsIntersectingWith(entity.getWorkerId(), from, to). //
        stream().map(this.mapper::toDomain).collect(Collectors.toList());

    final Map<LocalDate, Integer> counter = Maps.newHashMap();

    for (final Shift shift : shifts) {
      if (entity.intersects(shift)) {
        throw new IntersectingShiftException(shift.getId());
      }

      entity.intersectedDays(shift, offset). //
          forEach(day -> counter.put(day, counter.getOrDefault(counter, 0) + 1));
    }

    final Set<Integer> notAllowedNumberOfShifts = this.shiftConfig.getNotAllowedNumberOfShiftsAtTheSameDay().stream().map(
        i -> i - 1).collect(Collectors.toSet());
    for (final Entry<LocalDate, Integer> e : counter.entrySet()) {
      if (notAllowedNumberOfShifts.contains(e.getValue())) {
        throw new InvalidNumberOfShiftsAtTheSameDayException(e.getKey().toString());
      }
    }

    final ShiftDao dao = this.mapper.toDao(entity);
    final ShiftDao saved = this.repository.save(dao);
    return this.mapper.toDomain(saved);
  }

  /**
   * @see ro.treedev.teamway.bll.shift.ShiftService#delete(java.lang.String)
   */
  @Override
  public void delete(String id) {
    Preconditions.checkNotNull(id);
    this.repository.deleteById(id);
  }

  /**
   * @see ro.treedev.teamway.bll.DatabaseCleanupEventListener#handle(ro.treedev.teamway.bll.DatabaseCleanupEvent)
   */
  @Override
  public void handle(DatabaseCleanupEvent event) {
    try {
      this.repository.deleteAll();
    } catch (final Exception e) {
      log.error("Error while deleting all the shifts", e);
    }
  }

  /**
   * @see ro.treedev.teamway.bll.shift.ShiftService#handle(ro.treedev.teamway.bll.worker.WorkerDeletedEvent)
   */
  @Override
  public void handle(WorkerDeletedEvent event) {
    try {
      this.repository.deleteAllByWorkerId(event.getWorkerId());
    } catch (final Exception e) {
      log.error("Error while deleting the worker's shifts", e);
    }
  }

}
