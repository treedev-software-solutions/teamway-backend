/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.bll.shift;

import java.time.OffsetDateTime;
import java.util.Optional;
import java.util.stream.Stream;

import javax.json.JsonPatch;

import org.springframework.context.event.EventListener;

import ro.treedev.teamway.bll.DatabaseCleanupEventListener;
import ro.treedev.teamway.bll.worker.WorkerDeletedEvent;

public interface ShiftService extends DatabaseCleanupEventListener {

  Shift create(String workerId, OffsetDateTime startTime, OffsetDateTime endTime);

  Shift save(Shift entity);

  Optional<Shift> findById(String id);

  Shift getById(String id);

  Shift patch(String id, JsonPatch patch);

  Stream<Shift> getAll(String workerId);

  Stream<Shift> getAll();

  void delete(String id);

  @EventListener
  public void handle(WorkerDeletedEvent event);

}
