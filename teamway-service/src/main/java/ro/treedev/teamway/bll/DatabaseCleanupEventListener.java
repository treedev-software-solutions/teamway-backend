/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.bll;

import org.springframework.context.event.EventListener;

public interface DatabaseCleanupEventListener {

  @EventListener
  public void handle(DatabaseCleanupEvent event);

}
