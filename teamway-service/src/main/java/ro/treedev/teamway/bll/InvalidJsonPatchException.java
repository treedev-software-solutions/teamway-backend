/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.bll;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * Invalid Json patch exception.
 * 
 * @author Arnold Robert Turdean
 */
@Data
@ToString(callSuper = true)
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class InvalidJsonPatchException extends RuntimeException {

  /** serialVersionUID */
  private static final long serialVersionUID = 1L;

  /** DEFAULT_MESSAGE */
  private static final String DEFAULT_MESSAGE = "Invalid JSon patch operations";

  /**
   * Constructs a new instance.
   * 
   * @param message
   * @param cause
   * @param value
   */
  private InvalidJsonPatchException(String message, Throwable cause) {
    super(message, cause);
  }

  /**
   * Get the instance of UnknownUserException.
   * 
   * @param message
   * @param cause
   * 
   * @return
   */
  public static InvalidJsonPatchException of(String message, Throwable cause) {
    return new InvalidJsonPatchException(message, cause);
  }

  /**
   * Get the instance of UnknownUserException.
   * 
   * @param cause
   * @param message
   * 
   * @return
   */
  public static InvalidJsonPatchException of(Throwable cause) {
    return new InvalidJsonPatchException(DEFAULT_MESSAGE, cause);
  }

}
