/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * Application.
 * 
 * @author Arnold Robert Turdean
 */
@SpringBootApplication
@ComponentScan({ "ro.treedev" })
@EnableConfigurationProperties
@EnableMongoRepositories(basePackages = { "ro.treedev" })
@EnableMongoAuditing
@EnableCaching
public class Application {

  /**
   * Main.
   * 
   * @param args
   */
  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }

}
