/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway;

import java.time.ZoneOffset;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@Configuration
@ConfigurationProperties(prefix = "app")
public class AppConfig {

  private ZoneOffset zoneOffset = ZoneOffset.ofHours(3);

}
