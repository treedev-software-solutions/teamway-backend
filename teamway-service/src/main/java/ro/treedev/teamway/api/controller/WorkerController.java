/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.api.controller;

import java.util.stream.Stream;

import javax.json.JsonPatch;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import ro.treedev.teamway.api.worker.CreateWorkerDto;
import ro.treedev.teamway.api.worker.WorkerDto;
import ro.treedev.teamway.bll.worker.WorkerMapper;
import ro.treedev.teamway.bll.worker.WorkerService;

@RestController
public class WorkerController {

  public static final String POST = "v1/worker";

  public static final String GET = "v1/worker/{id}";

  public static final String GET_ALL = "v1/worker/getAll";

  public static final String PATCH = "v1/worker/{id}";
  
  public static final String DELETE = "v1/worker/{id}";

  @Autowired
  private WorkerService service;

  @Autowired
  private WorkerMapper mapper;

  @PostMapping(POST)
  public WorkerDto create(@Validated @RequestBody CreateWorkerDto dto) {
    return this.mapper.toDto(this.service.create(dto.getName()));
  }

  @GetMapping(GET)
  public WorkerDto get(@PathVariable String id) {
    return this.mapper.toDto(this.service.getById(id));
  }

  @GetMapping(GET_ALL)
  public Stream<WorkerDto> getAll() {
    return this.service.getAll().map(this.mapper::toDto);
  }

  @PatchMapping(path = PATCH, consumes = "application/json-patch+json")
  public WorkerDto patch(@PathVariable String id, @RequestBody JsonPatch jsonPatch) {
    return this.mapper.toDto(this.service.patch(id, jsonPatch));
  }
  
  @DeleteMapping(DELETE)
  public void delete(@PathVariable String id) {
    this.service.delete(id);
  }

}
