/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.api.controller;

import java.util.stream.Stream;

import javax.json.JsonPatch;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import ro.treedev.teamway.api.shift.CreateShiftDto;
import ro.treedev.teamway.api.shift.ShiftDto;
import ro.treedev.teamway.bll.shift.ShiftMapper;
import ro.treedev.teamway.bll.shift.ShiftService;

@RestController
public class ShiftController {

  public static final String POST = "v1/worker/{workerId}/shift";

  public static final String GET = "v1/worker/shift/{id}";

  public static final String GET_ALL = "v1/worker/shift/getAll";

  public static final String GET_ALL_BY_WOKRER_ID = "v1/worker/{workerId}/shift/getAll";

  public static final String PATCH = "v1/worker/shift/{id}";
  
  public static final String DELETE = "v1/worker/shift/{id}";

  @Autowired
  private ShiftService service;

  @Autowired
  private ShiftMapper mapper;

  @PostMapping(POST)
  public ShiftDto create(@PathVariable String workerId,
      @Validated @RequestBody CreateShiftDto dto) {
    return this.mapper.toDto(this.service.create(workerId, dto.getStartTime(), dto.getEndTime()));
  }

  @GetMapping(GET)
  public ShiftDto get(@PathVariable String id) {
    return this.mapper.toDto(this.service.getById(id));
  }

  @GetMapping(GET_ALL_BY_WOKRER_ID)
  public Stream<ShiftDto> getAll(@PathVariable String workerId) {
    return this.service.getAll(workerId).map(this.mapper::toDto);
  }

  @GetMapping(GET_ALL)
  public Stream<ShiftDto> getAll() {
    return this.service.getAll().map(this.mapper::toDto);
  }

  @PatchMapping(path = PATCH, consumes = "application/json-patch+json")
  public ShiftDto patch(@PathVariable String id, @RequestBody JsonPatch jsonPatch) {
    return this.mapper.toDto(this.service.patch(id, jsonPatch));
  }
  
  @DeleteMapping(DELETE)
  public void delete(@PathVariable String id) {
    this.service.delete(id);
  }

}
