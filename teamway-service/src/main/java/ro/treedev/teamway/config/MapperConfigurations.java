/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr353.JSR353Module;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;

import ro.treedev.teamway.config.jsonpatch.JohnzonModule;

/**
 * Holds mapper related configurations.
 * 
 * @author Arnold Robert Turdean
 */
@Configuration
public class MapperConfigurations {

  /**
   * Registers a custom {@link ObjectMapper} for serialization.
   *
   * @return
   */
  @Bean
  @Primary
  public ObjectMapper objectMapper() {
    final ObjectMapper mapper = new ObjectMapper(). //
        registerModule(new ParameterNamesModule()). //
        registerModule(new Jdk8Module()). //
        registerModule(new JavaTimeModule()). //
        registerModule(new JSR353Module()). //
        registerModule(JohnzonModule.INSTANCE). //
        configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false). //
        configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false). //
        configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false). //
        setSerializationInclusion(Include.NON_NULL);

    return mapper;
  }

}
