/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.config.jsonpatch;

import java.io.IOException;
import java.util.Iterator;

import javax.json.JsonArray;
import javax.json.JsonPatch;
import javax.json.JsonValue;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

/**
 * Serialize the given {@link JsonPatch}.
 * 
 * @author Arnold Robert Turdean
 */
public class JsonPatchSerializer extends JsonSerializer<JsonPatch> {

  /** INSTANCE */
  public static final JsonPatchSerializer INSTANCE = new JsonPatchSerializer();

  @Override
  public Class<JsonPatch> handledType() {
    return JsonPatch.class;
  }

  @Override
  public void serialize(JsonPatch value, JsonGenerator gen, SerializerProvider serializers)
      throws IOException {
    if (value != null) {
      gen.writeStartArray();

      final JsonArray jsonArray = value.toJsonArray();
      if (jsonArray != null) {

        final Iterator<JsonValue> it = jsonArray.iterator();
        while (it.hasNext()) {
          final JsonValue op = it.next();
          gen.writeRaw(op.toString());

          if (it.hasNext()) {
            gen.writeRaw(',');
          }
        }
      }

      gen.writeEndArray();
    }
  }

}
