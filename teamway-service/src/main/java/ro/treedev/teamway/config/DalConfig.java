/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.cache.CacheManager;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.data.mongodb.core.convert.MongoCustomConversions;
import org.springframework.web.context.WebApplicationContext;

import ro.treedev.teamway.dal.converter.DateToOffsetDateTimeConverter;
import ro.treedev.teamway.dal.converter.OffsetDateTimeToDateConverter;

/**
 * Data access layer configurations.
 * 
 * @author Arnold Robert Turdean
 */
@Configuration
public class DalConfig {

  /**
   * Registers the data access layer related converters.
   * 
   * @return
   */
  @Bean
  public MongoCustomConversions mongoCustomConversions() {
    final List<Object> list = new ArrayList<>();
    list.add(DateToOffsetDateTimeConverter.INSTANCE);
    list.add(OffsetDateTimeToDateConverter.INSTANCE);
    return new MongoCustomConversions(list);
  }
  
  @Bean
  @Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
  public CacheManager cacheManager() {
    return new ConcurrentMapCacheManager();
  }

}
