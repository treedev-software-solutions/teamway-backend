/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.config.jsonpatch;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.google.common.collect.Lists;

/**
 * Johnzon serialization/deserialization module. Note: Supports only the serialization.
 * 
 * @author Arnold Robert Turdean
 */
public class JohnzonModule extends SimpleModule {

  /** serialVersionUID */
  private static final long serialVersionUID = 1L;

  /** INSTANCE */
  public static final JohnzonModule INSTANCE = new JohnzonModule();

  /**
   * Constructs a new instance.
   */
  public JohnzonModule() {
    super("JohnzonModule", Version.unknownVersion(), //
        Lists.newArrayList(JsonPatchSerializer.INSTANCE));
  }

}
