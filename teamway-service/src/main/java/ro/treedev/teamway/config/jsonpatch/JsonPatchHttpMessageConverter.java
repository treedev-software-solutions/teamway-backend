/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.config.jsonpatch;

import java.io.IOException;

import javax.json.Json;
import javax.json.JsonPatch;
import javax.json.JsonReader;
import javax.json.JsonWriter;

import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.stereotype.Component;

/**
 * {@link JsonPatch} message converter.
 * 
 * @author Arnold Robert Turdean
 */
@Component
public class JsonPatchHttpMessageConverter extends AbstractHttpMessageConverter<JsonPatch> {

  /**
   * Constructs a new instance.
   */
  public JsonPatchHttpMessageConverter() {
    super(MediaType.valueOf("application/json-patch+json"));
  }

  /**
   * @see org.springframework.http.converter.AbstractHttpMessageConverter#supports(java.lang.Class)
   */
  @Override
  protected boolean supports(Class<?> clazz) {
    return JsonPatch.class.isAssignableFrom(clazz);
  }

  /**
   * @see org.springframework.http.converter.AbstractHttpMessageConverter#readInternal(java.lang.Class,
   *      org.springframework.http.HttpInputMessage)
   */
  @Override
  protected JsonPatch readInternal(Class<? extends JsonPatch> clazz, HttpInputMessage inputMessage)
      throws IOException, HttpMessageNotReadableException {
    try (JsonReader reader = Json.createReader(inputMessage.getBody())) {
      return Json.createPatch(reader.readArray());
    } catch (final Exception e) {
      throw new HttpMessageNotReadableException(e.getMessage(), inputMessage);
    }
  }

  /**
   * @see org.springframework.http.converter.AbstractHttpMessageConverter#writeInternal(java.lang.Object,
   *      org.springframework.http.HttpOutputMessage)
   */
  @Override
  protected void writeInternal(JsonPatch t, HttpOutputMessage outputMessage) throws IOException,
      HttpMessageNotWritableException {
    try (JsonWriter writer = Json.createWriter(outputMessage.getBody())) {
      writer.write(t.toJsonArray());
    } catch (final Exception e) {
      throw new HttpMessageNotWritableException(e.getMessage(), e);
    }
  }

}
