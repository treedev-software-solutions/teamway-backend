/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.error;

import com.google.common.base.Preconditions;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString(callSuper = false)
@Accessors(chain = true)
public class ApiError {

  private final String code;

  private final String message;

  private final String value;

  /**
   * Constructs a new instance.
   * 
   * @param code
   * @param message
   * @param parameter
   * @param value
   */
  public ApiError(String code, String message, String value) {
    Preconditions.checkNotNull(code);
    this.code = code;
    this.message = message;
    this.value = value;
  }

  /**
   * Constructs a new instance of {@link ApiError}.
   * 
   * @param <E>
   * @param code
   * @param message
   * @return
   */
  public static final ApiError of(String code, String message) {
    return new ApiError(code, message, null);
  }

  /**
   * Constructs a new instance of {@link ApiError}.
   * 
   * @param <E>
   * @param code
   * @param message
   * @param value
   * @return
   */
  public static final ApiError ofWithValue(String code, String message, String value) {
    return new ApiError(code, message, value);
  }

}
