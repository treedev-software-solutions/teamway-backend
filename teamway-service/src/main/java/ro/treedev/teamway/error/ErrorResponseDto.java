/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.error;

import java.util.List;

import com.google.common.collect.Lists;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * Default error response dto.
 * 
 * @author Arnold Robert Turdean
 * @param <E>
 */
@Data
@ToString(callSuper = true)
@Accessors(chain = true)
public class ErrorResponseDto {

  private List<ApiError> errors;

  public ErrorResponseDto() {
    // Noting.
  }

  /**
   * Constructs a new instance with one single error.
   * 
   * @param errorCode
   * @param message
   */
  public ErrorResponseDto(String errorCode, String message) {
    this(Lists.newArrayList(ApiError.of(errorCode, message)));
  }

  /**
   * Constructs a new instance with one single error.
   * 
   * @param errorCode
   * @param message
   * @param value
   */
  public ErrorResponseDto(String errorCode, String message, String value) {
    this(Lists.newArrayList(ApiError.ofWithValue(errorCode, message, value)));
  }

  /**
   * Constructs a new instance.
   * 
   * @param error
   */
  public ErrorResponseDto(ApiError error) {
    this(Lists.newArrayList(error));
  }

  /**
   * Constructs a new instance.
   * 
   * @param code
   * @param errors
   */
  public ErrorResponseDto(List<ApiError> errors) {
    this.errors = errors;
  }

}
