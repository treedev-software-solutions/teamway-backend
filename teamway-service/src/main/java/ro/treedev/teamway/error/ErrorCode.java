/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.error;

/**
 * Contains all the error codes.
 * 
 * @author Arnold Robert Turdean
 */
public enum ErrorCode {

  SERVER_ERROR(ErrorCodePriority.IMPORTANT),
  INVALID_REQUEST(ErrorCodePriority.WARNING),
  INVALID_PARAMETER(ErrorCodePriority.WARNING),
  UNSUPPORTED_REQUEST(ErrorCodePriority.WARNING),
  INVALID_JSON_PATCH(ErrorCodePriority.WARNING),

  // Worker
  UNKNOWN_WORKER,
  
  // Shift
  UNKNOWN_SHIFT,
  INTERSECTING_SHIFT,
  INVALID_NUMBER_OF_SHIFTS_AT_THE_SAME_DAY;

  private final ErrorCodePriority priority;

  private ErrorCode() {
    this.priority = ErrorCodePriority.DEBUG;
  }

  private ErrorCode(ErrorCodePriority priority) {
    this.priority = priority;
  }

  /**
   * Get priority.
   * 
   * @return priority
   */
  public ErrorCodePriority getPriority() {
    return this.priority;
  }

  public enum ErrorCodePriority {

    /** DEBUG */
    DEBUG,
    /** INFO */
    INFO,
    /** WARNING */
    WARNING,
    /** IMPORTANT */
    IMPORTANT;

  }
}
