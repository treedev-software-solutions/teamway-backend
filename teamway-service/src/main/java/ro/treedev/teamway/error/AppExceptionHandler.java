/*
 * Copyright (C) Treedev RO
 * All rights reserved.
 */
package ro.treedev.teamway.error;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Path;
import javax.validation.ValidationException;

import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.fasterxml.jackson.databind.JsonMappingException.Reference;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.extern.log4j.Log4j2;
import ro.treedev.teamway.bll.InvalidJsonPatchException;
import ro.treedev.teamway.bll.shift.IntersectingShiftException;
import ro.treedev.teamway.bll.shift.InvalidNumberOfShiftsAtTheSameDayException;
import ro.treedev.teamway.bll.shift.UnknownShiftException;
import ro.treedev.teamway.bll.worker.UnknownWorkerException;
import ro.treedev.teamway.core.error.ValuedRuntimeException;

@Log4j2
@RestControllerAdvice
public class AppExceptionHandler extends ResponseEntityExceptionHandler {

  private static final HttpHeaders HEADERS;

  private static final Map<Class<?>, ErrorCode> ERROR_CODE_CACHE;

  static {
    HEADERS = new HttpHeaders();
    HEADERS.setContentType(MediaType.APPLICATION_JSON);

    ERROR_CODE_CACHE = Maps.newHashMap();
    ERROR_CODE_CACHE.put(UnknownWorkerException.class, ErrorCode.UNKNOWN_WORKER);
    ERROR_CODE_CACHE.put(UnknownShiftException.class, ErrorCode.UNKNOWN_SHIFT);
    ERROR_CODE_CACHE.put(UnknownShiftException.class, ErrorCode.UNKNOWN_SHIFT);
    ERROR_CODE_CACHE.put(IntersectingShiftException.class, ErrorCode.INTERSECTING_SHIFT);
    ERROR_CODE_CACHE.put(InvalidNumberOfShiftsAtTheSameDayException.class,
        ErrorCode.INVALID_NUMBER_OF_SHIFTS_AT_THE_SAME_DAY);
  }

  @Override
  protected ResponseEntity<Object> handleServletRequestBindingException(
      ServletRequestBindingException e, HttpHeaders headers, HttpStatus status,
      WebRequest request) {
    final String msg = "Request binding problem";
    log.warn(msg, e);

    return new ResponseEntity<>(new ErrorResponseDto(ErrorCode.INVALID_REQUEST.name(), msg),
        HEADERS, status);
  }

  @Override
  protected ResponseEntity<Object> handleConversionNotSupported(ConversionNotSupportedException e,
      HttpHeaders headers, HttpStatus status, WebRequest request) {
    final String msg = "Unsupported conversion";
    log.warn(msg, e);

    return new ResponseEntity<>(new ErrorResponseDto(ErrorCode.INVALID_REQUEST.name(), msg),
        HEADERS, status);
  }

  @Override
  protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException e, HttpHeaders headers,
      HttpStatus status, WebRequest request) {
    final String msg = "Type mismatch";
    log.warn(msg, e);

    return new ResponseEntity<>(new ErrorResponseDto(ErrorCode.INVALID_REQUEST.name(), msg),
        HEADERS, status);
  }

  @Override
  protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException e,
      HttpHeaders headers, HttpStatus status, WebRequest request) {
    final List<ApiError> errors = Lists.newArrayList();

    if (e.getCause() instanceof InvalidFormatException) {
      final InvalidFormatException ife = (InvalidFormatException) e.getCause();

      final String problematicField = formatPathToProblematicField(ife.getPath());

      errors.add(new ApiError(ErrorCode.INVALID_PARAMETER.name(),
          "Request parameter cannot be parsed", problematicField));

      log.warn("There are problems with some of the request's arguments: " + problematicField);
    } else {
      log.warn("Unreadable message");
      errors.add(ApiError.of(ErrorCode.INVALID_REQUEST.name(), "Unreadable message"));
    }

    return new ResponseEntity<>(new ErrorResponseDto(errors), HEADERS, status);
  }

  /**
   * Formats the supplied References (which on their own point to a problematic field) into a human
   * readable path
   * 
   * @param fieldReferences
   * @return the input formatted as a {@link String}
   */
  private static String formatPathToProblematicField(Iterable<Reference> fieldReferences) {
    final StringBuilder sb = new StringBuilder();
    final Iterator<Reference> it = fieldReferences.iterator();
    if (it.hasNext()) {

      Reference current = it.next();
      boolean isArray = formatAndAppend(sb, current);
      while (it.hasNext()) {
        current = it.next();
        if (isArray || current.getIndex() < 0) {
          sb.append('.');
        }
        isArray = formatAndAppend(sb, current);
      }

    }
    return sb.toString();
  }

  /**
   * Formats and appends a given {@link Reference} to the supplied {@link StringBuilder}
   * 
   * @param sb
   * @param current
   * @return {@code true} if the current element which was appended was an array
   */
  private static boolean formatAndAppend(StringBuilder sb, Reference current) {
    final int index = current.getIndex();
    if (index >= 0) {
      sb.append('[').append(index).append(']');
      return true;
    } else {
      sb.append(current.getFieldName());
      return false;
    }
  }

  @Override
  public ResponseEntity<Object> handleExceptionInternal(Exception e, Object body,
      HttpHeaders headers, HttpStatus status, WebRequest request) {
    return this.handleThrowable(e, request);
  }

  @Override
  protected ResponseEntity<Object> handleBindException(BindException e, HttpHeaders headers,
      HttpStatus status, WebRequest request) {
    final List<String> invalidParams = Lists.newArrayList();
    final List<ApiError> errors = Lists.newArrayList();
    try {
      for (final FieldError error : e.getBindingResult().getFieldErrors()) {
        invalidParams.add(error.getField());
        errors.add(ApiError.ofWithValue(ErrorCode.INVALID_PARAMETER.name(),
            error.getDefaultMessage(), error.getField()));
      }
    } catch (final Exception ex) {
      log.error("Error while trying to get the invalid parameters.", ex);
    }

    log.warn("There are problems with some of the request's arguments: " + Joiner.on(",").join(
        invalidParams));

    return new ResponseEntity<>(new ErrorResponseDto(errors), HEADERS, status);
  }

  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException e,
      HttpHeaders headers, HttpStatus status, WebRequest request) {
    final List<String> invalidParamErrorMsgs = Lists.newArrayList();
    final List<ApiError> errors = Lists.newArrayList();
    try {
      for (final FieldError error : e.getBindingResult().getFieldErrors()) {
        invalidParamErrorMsgs.add(error.toString());

        errors.add(ApiError.ofWithValue(ErrorCode.INVALID_PARAMETER.name(),
            error.getDefaultMessage(), error.getField()));
      }

      for (final ObjectError globalError : e.getBindingResult().getGlobalErrors()) {
        errors.add(ApiError.of(ErrorCode.INVALID_REQUEST.name(), globalError.getDefaultMessage()));
      }
    } catch (final Exception ex) {
      log.error("Error while trying to get the invalid parameters.", ex);
    }

    log.warn("There are some invalid parameters: \n\t" + Joiner.on("\n\t").join(
        invalidParamErrorMsgs));

    return new ResponseEntity<>(new ErrorResponseDto(errors), HEADERS, status);
  }

  @Override
  protected ResponseEntity<Object> handleMissingPathVariable(MissingPathVariableException e,
      HttpHeaders headers, HttpStatus status, WebRequest request) {
    final String msg = "Missing path variable";

    log.warn(msg, e);

    return new ResponseEntity<>(new ErrorResponseDto(ErrorCode.INVALID_REQUEST.name(), msg),
        HEADERS, HttpStatus.BAD_REQUEST);
  }

  @Override
  protected ResponseEntity<Object> handleMissingServletRequestParameter(
      MissingServletRequestParameterException e, HttpHeaders headers, HttpStatus status,
      WebRequest request) {
    final String msg = "Misssing servlet request parameter: " + e.getParameterName();
    log.warn(msg);

    final ApiError error = ApiError.ofWithValue(ErrorCode.INVALID_PARAMETER.name(), msg,
        e.getParameterName());
    return new ResponseEntity<>(new ErrorResponseDto(error), HEADERS, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(value = { ConstraintViolationException.class })
  public ResponseEntity<Object> handleConstraintViolationException(ConstraintViolationException e,
      WebRequest request) {
    final StringJoiner errorMessages = new StringJoiner(" | ", "Validation errors ", ".");
    final List<ApiError> errors = Lists.newArrayList();

    for (final ConstraintViolation<?> violation : e.getConstraintViolations()) {
      final Path propertyPath = violation.getPropertyPath();
      if (propertyPath != null) {
        errors.add(ApiError.of(ErrorCode.INVALID_REQUEST.name(), //
            violation.getMessage()));
        errorMessages.add(violation.getMessage());
      } else {
        errors.add(ApiError.ofWithValue(ErrorCode.INVALID_PARAMETER.name(), //
            violation.getMessage(), //
            violation.getPropertyPath().toString()));
        errorMessages.add(violation.getPropertyPath().toString() + "-" + violation.getMessage());
      }
    }

    log.warn(errorMessages.toString());

    return new ResponseEntity<>(new ErrorResponseDto(errors), HEADERS, HttpStatus.BAD_REQUEST);
  }

  /**
   * @see org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler#handleNoHandlerFoundException(org.springframework.web.servlet.NoHandlerFoundException,
   *      org.springframework.http.HttpHeaders, org.springframework.http.HttpStatus,
   *      org.springframework.web.context.request.WebRequest)
   */
  @Override
  protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException e,
      HttpHeaders headers, HttpStatus status, WebRequest request) {
    final String msg = "No handler found for the given URI: " + e.getRequestURL();
    log.warn(msg);

    return new ResponseEntity<>(new ErrorResponseDto(ErrorCode.INVALID_REQUEST.name(), msg),
        HEADERS, status);
  }

  /**
   * @see org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler#handleHttpRequestMethodNotSupported(org.springframework.web.HttpRequestMethodNotSupportedException,
   *      org.springframework.http.HttpHeaders, org.springframework.http.HttpStatus,
   *      org.springframework.web.context.request.WebRequest)
   */
  @Override
  protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(
      HttpRequestMethodNotSupportedException e, HttpHeaders headers, HttpStatus status,
      WebRequest request) {
    final String msg = "No handler found for URI: "
        + ((ServletWebRequest) request).getRequest().getRequestURI() + " with type: "
        + e.getMethod() + ". Supported types: " + e.getSupportedHttpMethods();

    log.warn(msg);

    return new ResponseEntity<>(new ErrorResponseDto(ErrorCode.INVALID_REQUEST.name(), msg),
        HEADERS, status);
  }

  /**
   * Handle all the exceptions.
   * 
   * @param e
   * @param request
   * @return
   */
  @ExceptionHandler(value = { Throwable.class })
  public ResponseEntity<Object> handleThrowable(Throwable e, WebRequest request) {
    return this.handle(e, request);
  }

  /**
   * Handle {@link HttpMessageConversionException}.
   * 
   * @param e
   * @param request
   * @return
   */
  @ExceptionHandler(value = { HttpMessageConversionException.class })
  public ResponseEntity<Object> handleMessageConversionException(HttpMessageConversionException e,
      WebRequest request) {
    final String msg = "The message cannot be converted";

    log.warn(msg);

    return new ResponseEntity<>(new ErrorResponseDto(ErrorCode.INVALID_REQUEST.name(), msg),
        HEADERS, HttpStatus.BAD_REQUEST);
  }

  /**
   * Handle {@link MethodArgumentTypeMismatchException}.
   * 
   * @param e
   * @param request
   * @return
   */
  @ExceptionHandler(value = { MethodArgumentTypeMismatchException.class })
  public ResponseEntity<Object> handleMethodArgumentTypeMismatch(
      MethodArgumentTypeMismatchException e, WebRequest request) {
    final String msg = "Invalid argument type: " + e.getName();
    final ApiError error = ApiError.ofWithValue(ErrorCode.INVALID_PARAMETER.name(), msg,
        e.getName());

    log.warn(msg);

    return new ResponseEntity<>(new ErrorResponseDto(error), HEADERS, HttpStatus.BAD_REQUEST);
  }

  /**
   * Handle {@link ValidationException}.
   * 
   * @param e
   * @param request
   * @return
   */
  @ExceptionHandler(value = { ValidationException.class })
  public ResponseEntity<Object> handleValidationException(ValidationException e,
      WebRequest request) {
    if (e.getCause() != null) {
      return this.handle(e.getCause(), request);
    } else {
      return this.handle(e, request);
    }
  }

  /**
   * Handle {@link InvalidJsonPatchException}.
   * 
   * @param e
   * @param request
   * @return
   */
  @ExceptionHandler(value = { InvalidJsonPatchException.class })
  public ResponseEntity<Object> handleInvalidJsonPatchException(InvalidJsonPatchException e,
      WebRequest request) {
    final Throwable cause = e.getCause();
    if (cause instanceof ConstraintViolationException) {
      return this.handleConstraintViolationException((ConstraintViolationException) cause, request);
    } else if (cause instanceof ValidationException) {
      return this.handleValidationException((ValidationException) cause, request);
    } else {
      return this.handle(e.getCause(), request, HttpStatus.BAD_REQUEST);
    }
  }

  /**
   * @see org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler#handleMissingServletRequestPart(org.springframework.web.multipart.support.MissingServletRequestPartException,
   *      org.springframework.http.HttpHeaders, org.springframework.http.HttpStatus,
   *      org.springframework.web.context.request.WebRequest)
   */
  @Override
  protected ResponseEntity<Object> handleMissingServletRequestPart(
      MissingServletRequestPartException ex, HttpHeaders headers, HttpStatus status,
      WebRequest request) {
    final String msg = "Request part is missing!";
    log.warn(msg, ex);

    return new ResponseEntity<>(new ErrorResponseDto(ErrorCode.INVALID_PARAMETER.name(),
        "Request part is missing!"), HEADERS, status);
  }

  /**
   * Handle the given exception and create the error response for the client.
   * 
   * @param code
   * @param e
   * @param request
   * @return
   */
  public ResponseEntity<Object> handle(Throwable e, WebRequest request) {
    return this.handle(e, request, null);
  }

  /**
   * Handle the given exception and create the error response for the client.
   * 
   * @param code
   * @param e
   * @param request
   * @param preDefinedStatus
   * @return
   */
  public ResponseEntity<Object> handle(Throwable e, WebRequest request,
      HttpStatus preDefinedStatus) {
    final ErrorCode errorCode = ERROR_CODE_CACHE.getOrDefault(e.getClass(), ErrorCode.SERVER_ERROR);
    this.logError(e, errorCode);

    final String respMessage = errorCode != ErrorCode.SERVER_ERROR ? e.getMessage()
        : "Unknown server error!";
    final ErrorResponseDto response;
    if (e instanceof ValuedRuntimeException && ((ValuedRuntimeException) e).getValue() != null) {
      response = new ErrorResponseDto(errorCode.name(), respMessage,
          ((ValuedRuntimeException) e).getValue());
    } else {
      response = new ErrorResponseDto(errorCode.name(), respMessage);
    }

    return new ResponseEntity<>(response, HEADERS, this.getStatus(errorCode, preDefinedStatus));
  }

  private void logError(Throwable e, ErrorCode errorCode) {
    switch (errorCode.getPriority()) {
    case DEBUG:
      log.debug(e.getMessage(), e);
      break;
    case INFO:
      log.info(e.getMessage(), e);
      break;
    case WARNING:
      log.warn(e.getMessage(), e);
      break;
    case IMPORTANT:
      log.error(e.getMessage(), e);
      break;
    default:
      log.error(e.getMessage(), e);
      break;
    }
  }

  private HttpStatus getStatus(ErrorCode errorCode, HttpStatus preDefinedStatus) {
    if (errorCode == ErrorCode.SERVER_ERROR) {
      return HttpStatus.INTERNAL_SERVER_ERROR;
    } else {
      return preDefinedStatus != null ? preDefinedStatus : HttpStatus.INTERNAL_SERVER_ERROR;
    }
  }

}
